package com.dashen.controller;

import com.dashen.model.Car;

import java.sql.*;
import java.util.List;

public class CarRepository {

    public static Connection connection = null;

    static PreparedStatement connectionPrepareStat = null;

    public static void makeJDBCConnection() {

        System.out.println("**********************start********************************");
        try {

            Class.forName("org.h2.Driver");

        } catch (ClassNotFoundException e) {

            e.printStackTrace();

            return;

        }
        System.out.println("***********************loaded driver********************************");

        try {

            if (connection == null) {

                System.out.println("***********************connecting to server********************************");

                connection = DriverManager.getConnection("jdbc:h2:mem:AZ;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE", "sa", "");

                System.out.println("***********************connected to server********************************");

            }
        } catch (SQLException e) {

            e.printStackTrace();

            return;

        }

    }

    public void insertCar(Car car) throws SQLException {

        String q = "INSERT  INTO  Car  VALUES  (?,?,?,?,?,?,?)";

        if(connection == null)
        {
            System.out.println("No connection");

            return;

        }
        connectionPrepareStat = connection.prepareStatement(q);

        connectionPrepareStat.setInt(1, car.getId());
        connectionPrepareStat.setString(2, car.getMake());
        connectionPrepareStat.setInt(3, car.getYear());
        connectionPrepareStat.setString(4, car.getCondition());

        connectionPrepareStat.setString(5, car.getModel());

        connectionPrepareStat.setString(6, car.getTransmision());
        connectionPrepareStat.setFloat(7, car.getPrice());

        connectionPrepareStat.execute();

    }

    public List<Car> readAll() throws SQLException {

        List<Car> ret = null;

        String q = "SELECT * FROM CAR";

        connectionPrepareStat = connection.prepareStatement(q);

        ResultSet resultSet = connectionPrepareStat.executeQuery();

        return ret;
    }

    public void update(Car car) throws SQLException {

        String q = "UPDATE T" +
                "   SET make = ?"+
                        "year = ?"+
                        "condition = ?"+
                        "model = ?"+
                        "transmission = ?"+
                        "price = ?"+
                " WHERE id = ?";

        connectionPrepareStat = connection.prepareStatement(q);

        connectionPrepareStat.setString(1, car.getMake());
        connectionPrepareStat.setInt(2, car.getYear());
        connectionPrepareStat.setString(3, car.getCondition());

        connectionPrepareStat.setString(4, car.getModel());

        connectionPrepareStat.setString(5, car.getTransmision());
        connectionPrepareStat.setFloat(6, car.getPrice());

        connectionPrepareStat.setInt(7, car.getId());

        // execute insert SQL statement
        connectionPrepareStat.executeUpdate();

    }

    public Car delete(Car car) throws SQLException {

        Car ret = null;

        String q = "DELETE FROM CAR WHERE ID =?";

        connectionPrepareStat = connection.prepareStatement(q);

        connectionPrepareStat.setInt(1, car.getId());

        ResultSet resultSet =  connectionPrepareStat.executeQuery();

        return ret;
    }

    public Car read(int car) throws SQLException {

        Car ret = null;

        String q = "DELETE FROM CAR WHERE ID =?";

        connectionPrepareStat = connection.prepareStatement(q);

        connectionPrepareStat.setInt(1, car);

        ResultSet resultSet =  connectionPrepareStat.executeQuery();

        return ret;
    }

}
