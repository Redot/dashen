package com.dashen.model;

public class Car {

    private int Id;

    private final String make;

    private final String model;

    private final int year;

    private final String transmision;

    private final String condition;

    private final float price;

    public Car(int id, String make, String model, int year, String transmision, String condition, float price) {
        Id = id;
        this.make = make;
        this.model = model;
        this.year = year;
        this.transmision = transmision;
        this.condition = condition;
        this.price = price;
    }

    public Car( String make, String model, int year, String transmision, String condition, float price) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.transmision = transmision;
        this.condition = condition;
        this.price = price;
    }


    public int getId() {
        return Id;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public int getYear() {
        return year;
    }

    public String getTransmision() {
        return transmision;
    }

    public String getCondition() {
        return condition;
    }

    public float getPrice() {
        return price;
    }
}
