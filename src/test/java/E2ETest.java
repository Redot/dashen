import com.dashen.controller.CarRepository;
import com.dashen.model.Car;
import oracle.jrockit.jfr.tools.ConCatRepository;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;

public class E2ETest {


    CarRepository carRepository = new CarRepository();
//create car Tabel
    @Before
    public void setup() throws SQLException {

        System.out.println("*********************************************table created*********************************************");

        CarRepository.makeJDBCConnection();

        String q = "CREATE TABLE Car ( id int, " +
                "make varchar(25)," +
                " model varchar(25)," +
                "year int, " +
                " transmision varchar(25), " +
                "condition varchar(25) ," +
                "price FLOAT(12) );";

        try {
            CarRepository.connection.prepareStatement(q).execute();
        }catch(Exception e){}
    }


//    insert car

    @Test
    public void inserttest() {

        System.out.println("*********************************************Inserted*********************************************");

        CarRepository carRepository = new CarRepository();

        CarRepository.makeJDBCConnection();

        Car car = new Car(1,
                "Toyota",
                "Corolla",
                2007,
                "Automatic",
                "Used",
                830000f);

        Car car2 = new Car(2,
                "Nissan",
                "Kicks",
                2019,
                "Automatic",
                "New",
                2000000f);

        try {

            carRepository.insertCar(car);

            carRepository.insertCar(car2);

        }catch (Exception e){

            e.printStackTrace();

        }

        System.out.println("*********************************************Inserted*********************************************");

    }

//update
    @Test
    public void updatetest() {

        CarRepository carRepository = new CarRepository();

        CarRepository.makeJDBCConnection();

        Car car = new Car(1,"make",  "model", 2012, "transmision",  "newCar", 500000.15f);

        try {

            carRepository.update(car);

        }catch (Exception e){

            e.printStackTrace();

        }

        System.out.println("*********************************************updated*********************************************");


    }

//    display
    @Test
    public void dsiplaytest() {

        CarRepository carRepository = new CarRepository();

        CarRepository.makeJDBCConnection();

        try {

            carRepository.read(1);

        }catch (Exception e){

            e.printStackTrace();

        }

        System.out.println("*********************************************Display*********************************************");


    }

//read all
    @Test
    public void dsiplayAlltest() {

        CarRepository carRepository = new CarRepository();

        CarRepository.makeJDBCConnection();

        try {

            carRepository.read(1);

        }catch (Exception e){

            e.printStackTrace();

        }

        System.out.println("*********************************************Display all*********************************************");


    }


}
